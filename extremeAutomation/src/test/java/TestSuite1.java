package test.java;

import main.java.Randomiser;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created by vladyslav on 2/19/17.
 */
public class TestSuite1 {
    @Test
    public void RandomiserTest1(){
        int result;
        Randomiser number=new Randomiser();
        result=number.getRandomNumber(0,9999);
        assertTrue(result>(-1));
    }

    @Test
    public void RandomiserTest2(){
        int result;
        Randomiser number=new Randomiser();
        result=number.getRandomNumber(0,9999);
        assertTrue(result<10000);
    }

    @Test
    public void RandomiserTest3(){
        int result;
        Randomiser number=new Randomiser();
        result=number.getRandomNumber(0,9999);
        assertNotNull(result);


    }
}
